function git-df --wraps='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' --description 'alias git-df=/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
  /usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME $argv; 
end
